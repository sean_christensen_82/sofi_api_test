**This project uses the frame work frisby.**
http://frisbyjs.com/

**To Run Tests**
Frisby is built on top of the Jasmine BDD framework, and uses the jasmine-node
test runner to run spec tests.

**Install jasmine-node**

* npm install -g jasmine-node

**File naming conventions**
Files must end with spec.js to run with jasmine-node.
Suggested file naming is to append the filename with _spec, like mytests_spec.js and moretests_spec.js

**Add your developer api_key for www.themoviedb.org**
In the folder assets (your/project/assets) add your developer key to the 'api_key': ''

**Run it from the CLI**
cd **your/project/** tests/
jasmine-node apiTest_spec.js