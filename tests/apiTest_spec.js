
var frisby = require('frisby');
var apiKey = require('../assets/args').apiKey;
var path = require('../assets/args').path;
var user = require('../assets/args').user;

var request_token = '';
var session_id = '';
var guest = '';


//TODO: Verify that the Create Request Token GET call return correct statuses when given correct and incorrect data.
/* The GET calls below verify that the correct statuses are returned when a user does not have a correct api_token (401)
 and a valid api_token (200). This is the first call that is made to validate a user, its important to make sure that your developer
 api_token is valid and that its required when generating a valid session.
 */
//The GET call below passes a invalid (missing) api_token into the Create_Request_Token api (GET/authentication/token/new)
frisby.create('request token without a valid api_token')
    .get(path.hostname + path.userSession)
    .expectJSONTypes({
        'status_message': String,
        'status_code': Number
    })
    .expectStatus(401)
    .toss();

//The GET call below passes a valid api_token into the Create_Request_Token api (GET/authentication/token/new)
frisby.create('request token')
    .get(path.hostname + path.userSession + apiKey.api_key)
    .expectJSONTypes({
        'success': Boolean,
        'expires_at': String,
        'request_token': String
    })
    .expectStatus(200)
    .expectHeader('Content-Type', 'application/json;charset=utf-8')
    .afterJSON(function (json) {
        request_token = json ['request_token'];


//TODO: Verify that the Validate Request Token Get call returns the correct statuses when given correct and incorrect user information.
        /* The GET calls below verify that without correct user credentials the server sends back a 401 status and will not validate the request_token.
         The test makes three calls to the server one with an expired request_token, another with a invalid password, and the last with a invalid username.
         This will verify that having two of the three correct will still not validate the request token
         */
//The Get Calls below pass invalid user information into the Validate_Request_Token api (GET/authentication/token/validate_with_login)
        frisby.create('validate request token with invalid user credentials')
            .get(path.hostname + '/authentication/token/validate_with_login?api_key=' + apiKey.api_key + '&username=' + user.username + '&password=' + user.password + '&request_token=' + path.expiredRequestToken)
            .expectStatus(401)
            .get(path.hostname + '/authentication/token/validate_with_login?api_key=' + apiKey.api_key + '&username=' + user.username + '&password=' + user.invalidPassword + '&request_token=' + path.expiredRequestToken)
            .expectStatus(401)
            .get(path.hostname + '/authentication/token/validate_with_login?api_key=' + apiKey.api_key + '&username=' + user.invalidUserName + '&password=' + user.password + '&request_token=' + request_token)
            .expectJSONTypes({
                'status_message': String,
                'status_code': Number
            })
            .expectJSON({
                'status_message': 'Invalid username and/or password: You did not provide a valid login.',
                'status_code': 30
            })
            .expectStatus(401)
            .toss();
//The Get Calls below pass invalid user information into the Validate_Request_Token api (GET/authentication/token/validate_with_login)
        frisby.create('validate request token with valid user credentials')
            .get(path.hostname + '/authentication/token/validate_with_login?api_key=' + apiKey.api_key + '&username=' + user.username + '&password=' + user.password + '&request_token=' + request_token)
            .expectJSONTypes({
                'success': Boolean,
                'request_token': String
            })
            .expectJSON({
                'success': true,
                'request_token': request_token
            })
            .expectStatus(200)
            .toss();

//TODO: Verify that the Create Session GET call returns the correct statuses for validated and invalidated request_tokens
        /*The GET calls below verify that if the two previous API calls where successful that the server returns a valid session_id
         and if they were not successful or if the request_token was not validated that the server doesn't return a valid session_id.
         Testing that all previous calls returned successful by creating a valid session_id verify that going forward this user can leave reviews/ratings
         and that if anywhere along the way a step failed, was spoofed, or was skipped results in a 401 status.
         */
//The Get Call below pass a invalid request_token into the Create Session api (GET/authentication/session/new)
        frisby.create('session with an expired request token ')
            .get(path.hostname + '/authentication/session/new?api_key=' + apiKey.api_key + '&request_token=' + path.expiredRequestToken)
            .expectJSONTypes({
                'status_message': String,
                'status_code': Number
            })
            .expectJSON({
                'status_message': 'Session denied.',
                'status_code': 17
            })
            .toss();

//The Get Call below pass a valid request_token into the Create Session api (GET/authentication/session/new)
        frisby.create('create session')
            .get(path.hostname + '/authentication/session/new?api_key=' + apiKey.api_key + '&request_token=' + request_token)
            .expectJSONTypes({
                'success': Boolean,
                'session_id': String
            })
            .expectJSON({
                'success': true,
                'session_id': json['session_id']
            })
            .afterJSON(function (json) {
                session_id = json['session_id']

            })
            .toss();


//TODO: Verify that the Create Guest Session GET call returns correctly when given correct and incorrect data.
        /*The Get call below will generate a valid guest_session_id if all the credentials are correct. If we allow for guest accounts this test
         will insure that a guest user can generate a session_id and leave ratings without creating an account and that a user would have to have
         our valid api_key inorder to post ratings
         */
//The Get Call below pass a invalid api_key (missing) into the Create Guest Session api (GET/authentication/guest_session/new)
        frisby.create('guest session id with invalid api_key')
            .get(path.hostname + path.guestSession)
            .expectHeader('Content-Type', 'application/json;charset=utf-8', 'status', '200 ok')
            .expectJSON({
                'status_message': 'Invalid API key: You must be granted a valid key.',
                'status_code': 7
            })
            .expectJSONTypes({
                'status_message': String,
                'status_code': Number
            })
            .expectStatus(401)
            .toss();
//The Get Call below pass a valid api_key into the Create Guest Session api (GET/authentication/guest_session/new). This generates the guest_session_id
        frisby.create('guest session id with valid api_key')
            .get(path.hostname + path.guestSession + apiKey.api_key)
            .expectHeader('Content-Type', 'application/json;charset=utf-8', 'status', '200 ok')
            .expectJSON({'success': true})
            .expectJSONTypes({
                'success': Boolean,
                'guest_session_id': String,
                'expires_at': String
            })
            .expectStatus(200)
            .afterJSON(function (json) {
                guest = json ['guest_session_id'];


//TODO: Verify that when an un-authenticated user attempts to post a rating to a movie the Rate Movie Post returns correctly
                /*This test will verify that if a user does not have a valid request token, they are unable to post a review on a movie.
                 Because the site only wants verified reviews from its users, we want to verify that a non-user authenticated user is unable
                 to leave movie reviews.
                 */
                frisby.create('attempt to post a review with missing/incorrect credentials, verify the 401 response')
                    .post(path.hostname + '/movie/'+path.movieId+'/rating?api_key='  + apiKey.api_key, {
                        'guest_session_id': path.notValidGuest,
                        'value': 8.5
                    })
                    .expectHeader('Content-Type', 'application/json;charset=utf-8', 'status', '401 Unauthorized')
                    .expectJSONTypes({
                        'status_message': String,
                        'status_code': Number
                    })
                    .expectJSON({
                        'status_message': 'Authentication failed: You do not have permissions to access the service.',
                        'status_code': 3
                    })
                    .expectStatus(401)
                    .toss();


//    //TODO: Verify that when a user with an expired session token attempts to post a movie rating the Rate Movie Post returns correctly.
                /*By sending an expired session id we can test  that the sever will return a 404 un-authorized in the place of a 200. This will insure that
                 our expires at flag is working correctly and that user that have been inactive can't post movie ratings.
                 */
                /*      frisby.create('guest_session_id and get expires_at time')
                 .post('path.hostname+'/movie/1114/rating?api_key=' + apiKey.api_key, {
                 pass in a valid guest_session_id
                 })
                 .expectStatus(200)
                 .afterJSON(function(json){
                 Once the call is complete get the following after the returned JSON:
                 guest_session_id:
                 expires_at:

                 create a Date() function to get the current date in the format of YYYY-MM-DD HH:MM:SS UTC

                 expect(json['expires_at'].toBeGreaterThen(the returned date from the date function)
                 Use the valid guest_session_id and attempt to post with the expired date set to current date and time

                 frisby.create('guest_session_id and get expires_at time')
                 .post('https://api.themoviedb.org/3/movie/1114/rating?api_key=' + apiKey.api_key, {
                 'guest_session_id': json[''],

                 set the expires_at to the returned current date
                 'expires_at: 'returned current date time stamp'
                 })
                 .expectStatus(404)
                 .expectJSON{
                 add the expected outputs
                 }
                 */

//TODO: Verify that a user with a valid guest_session_id or valid session_id attempting to POST to Rate Movie where the movie id is invalid or missing returns correctly .
                /*This test will attempt to post a review to a movie that does not exist on the database. By attempting
                 to post a review to a missing resource. This will verify that the system can gracefully handle valid authenticated
                 requests to a missing resource.
                 */
                frisby.create('post a review to a non-existing movie with guest_session_id')
                    .post(path.hostname + '/movie/'+path.notMovieId+'/rating?api_key=' + apiKey.api_key, {
                        'guest_session_id': guest,
                        'value': 8.5
                    })
                    .expectStatus(404)
                    .afterJSON(function () {
                        frisby.create('post a review to a non-existing movie with session_id')
                            .post(path.hostname + '/movie/'+path.notMovieId+'/rating?api_key='  + apiKey.api_key, {
                                'session_id': session_id,
                                'value': 8.5
                            })

                            .expectHeader('Content-Type', 'application/json;charset=utf-8', 'status', '401 Unauthorized')
                            .expectJSONTypes({
                                'status_message': String,
                                'status_code': Number
                            })
                            .expectJSON({
                                'status_message': 'The resource you requested could not be found.',
                                'status_code': 34
                            })
                            .expectStatus(404)
                            .toss()
                    })
                    .toss();

//TODO: Verily that a user with a valid guest_session_id or session_id can post a movie review and the Rate Movie POST returns correctly
                /* Test verifies that a user that is authenticated can post a movie review and that the server returns the correct status and
                 return messages. By passing a valid gguest_session_id or session_id and a value between .05 and 10 we can verify that a review is
                 posted to the database.
                 */
                frisby.create('post a review to the site with a valid guest_session_id')
                    .post(path.hostname + '/movie/'+path.movieId+'/rating?api_key='  + apiKey.api_key, {
                        'guest_session_id': guest,
                        'value': 8.5
                    })
                    .expectStatus(201)
                    .afterJSON(function () {
                        frisby.create('post a review to the site with a valid seesion_id')
                            .post(path.hostname + '/movie/'+path.movieId+'/rating?api_key='  + apiKey.api_key, {
                                'session_id': session_id,
                                'value': 8.5
                            })
                            .expectHeader('Content-Type', 'application/json;charset=utf-8', 'status', '201 Created')
                            .expectJSONTypes({
                                'status_message': String,
                                'status_code': Number
                            })
                            .expectJSON({
                                'status_message':  'The item/record was updated successfully.',
                                'status_code': 12
                            })
                            .expectStatus(201)
                            .toss();
                    })
                    .toss();
//TODO: Verily that a user with a valid guest_session_id or session_id can only send numbers between 0.5 - 10 and the Rate Movie POST returns correctly
                /*Verify that the Rate Movie POST (POST/movie/{movie_id}/rating) will only except numbers between .05-10  and multiples of .50 by sending numbers higher and lower and by sending letters
                 and special character. This will allow us to see if the  algorithm on the front end that splits the 5 stars into numbers is correct and only sending .05 - 10 and that
                 the backend is only excepting numbers in the range.
                 */
                var inputs = [.01, 11, 'Two', '@'];
                var status_message = ['Value invalid: Values must be a multiple of 0.50.', 'Value too high: Value must be less than, or equal to 10.0.',
                    'Value too low: Value must be greater than 0.0.', 'Value too low: Value must be greater than 0.0.'];
                for (var i = 0; i <= 3; i++) {
                    frisby.create('post a review to the site with a valid guest_session_id')
                        .post(path.hostname + '/movie/'+path.movieId+'/rating?api_key=' + apiKey.api_key, {
                            'guest_session_id': guest,
                            'value': inputs[i]
                        })
                        .expectJSONTypes({
                            'status_message': String,
                            'status_code': Number
                        })
                        .expectJSON({
                            'status_message': status_message [i],
                            'status_code': 18
                        })
                        .expectStatus(400)
                        .toss();
                }

//TODO: Verify that the DELETE Delete Rating call returns correctly when passed correct user and movie_id information.
                /*Verify that a authenticated user can delete a movie rating (DELETE/movie/{movie_id}/rating) that they
                 had previously left on a movie. By adding a correct movie_id to the url (a movie that the user has rated in the past, this is done in the above tests) and
                 by passing the correct session_id or guest_session_id we can test that a user can delete a rating and that the JSON returns correctly.
                 */
                /*              frisby.create('a test that verifies the delete rating call'
                 .delete(path.hostname/movie/ ** add a movie id that the user has left a rating for *** (use same movie )id from above tests that left a review/rating?api_key=' + apiKey.api_key, {
                 .expectJSONTypes{
                 add the types
                 }
                 .expectJSON{
                 add the status messages and the status code that is expected
                 }
                 repeat or loop through test again to verify the opposite user type (guest user / user)
//TODO: Verify that the DELETE Delete Rating call returns correctly when an authenticated user attempts to delete a rating to a movie that they have not rated previously
                 /*Verify that a authenticated user can not  delete a movie rating (DELETE/movie/{movie_id}/rating) that they
                 have not previously left on a movie. By adding an incorrect movie_id to the url (a movie_id to a movie that the user has not rated in the past) and
                 by passing the correct session_id or guest_session_id we can test that a user can't delete a rating to a movie they have not rated and the JSON returns correctly.
                 */
                /*frisby.create('a test that verifies the delete rating call'
                 .delete(path.hostname/movie/ ** add a movie id that the user has not left a rating for api_key=' + apiKey.api_key, {
                 .expectJSONTypes{
                 add the types
                 }
                 .expectJSON{
                 add the status messages and the status code that is expected
                 }
                 repeat or loop through test again to verify the opposite user type (guest user / user)
                 */
            })
//TODO: find a better way to store and use the guest_session_id and session_id globally so that the below toss() calls can be nested better
            //The below toss() allows us to use the guest_session_id globally
            .toss();
        //The below toss() allows us to use the session_id globally
    })
    .toss();








