/**
 * Created by i54607 on 4/11/17.
 */

module.exports = {

    apiKey: {
//******** Replace the api_key below with your own developer api_key ************************
        'api_key': ''
    },

    user: {
        'username': 'sofiTestUser_sean',
        'password': 'w2eR09In97',
        'invalidUserName': 'thisIsNotARealUserName',
        'invalidPassword': 'notAValidPassword',
        'notValidGuest': 'notAValidGuestSessionId',
        'expiredRequestToken': '9db782a4b99fc35b5398ffe35f9a06718fd1d263'
    },
    path: {
        'hostname': 'https://api.themoviedb.org/3',
        'userSession': '/authentication/token/new?api_key=',
        'guestSession': '/authentication/guest_session/new?api_key=',
        'movieRatingPath': '/movie/1114/rating?api_key=',
        'moviePath': '/movie/1114?api_key=',
        'movieId': '135397',
        'notMovieId': '0000'
    }
};

